package pl.quider.standalone.irc;

import pl.quider.standalone.irc.bot.model.User;

import java.util.Date;

public class TestUtils {

    public static User prepareUserMock() {
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setLastSeen(new Date());
        mockUser.setLogin("login");
        mockUser.setMask("hostname");
        mockUser.setNickName("nick");
        mockUser.setOp(true);
        return mockUser;
    }
}
