package pl.quider.standalone.irc.plugins.enshortLink.verb;

import org.dom4j.DocumentException;
import org.xml.sax.SAXException;
import pl.quider.standalone.irc.bot.events.MessageReadyToSend;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.services.EnshortLinkService;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.net.MalformedURLException;

/**
 * Every enshortLinks is applied to every line sent by another users on channel
 */
public class EnshortLink extends Verb {


    /**
     * Reads xmls:
     * <?xml version="1.0" encoding="UTF-8" ?>
     * <message client="92.60.129.178" time="1476366442">
     *  <response>
     *      <code>200</code>
     *      <data>
     *          <url_id>626119</url_id>
     *          <short_url>http://xurl.pl/fdjN</short_url>
     *          <short_url_part>fdjN</short_url_part>
     *          <original_url>http://openweathermap.org/appid</original_url>
     *          <date_time>2016-10-13 15:47:22</date_time>
     *      </data>
     *  </response>
     * </message>
     */

    public void execute(Message msg) throws Exception {

        try {
            EnshortLinkService filer = new EnshortLinkService();
            String url = filer.generateShortUrl(msg.getMessage());
            applicationEventPublisher.publishEvent(new MessageReadyToSend(this, msg.getChannel(), url));

        } catch (MalformedURLException | DocumentException | SAXException e) {
            e.printStackTrace();
        }
    }
}
