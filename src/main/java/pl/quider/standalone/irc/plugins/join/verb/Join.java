package pl.quider.standalone.irc.plugins.join.verb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.quider.standalone.irc.bot.events.JoinChannelEvent;
import pl.quider.standalone.irc.bot.events.MessageArrived;
import pl.quider.standalone.irc.bot.events.ProtectedMessageArrived;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.services.ChannelService;
import pl.quider.standalone.irc.bot.services.MessageService;
import pl.quider.standalone.irc.bot.verb.Verb;

/**
 * Verb class which is responsible for action of join to channel
 */
@Component
@Slf4j
public class Join extends Verb {

    @Value("${bot.join_enabled}")
    private String joinEnabled;
    @Autowired
    private MessageService messageService;
    @Autowired
    private ChannelService channelService;

    @EventListener()
    public void execute(MessageArrived parameter) {
        handleMessage(parameter.getMessage());
    }

    protected void handleMessage(Message message) {
        if (!joinEnabled.equals("1")) {
            return;
        }
        String text = message.getMessage();
        if (!messageService.isUserCallingMe(text)) {
            return;
        }
        String[] split = message.getMessage().split(" ");
        if (split.length >= 3)
            for (int i = 2; i <= split.length - 1; i++) {
                this.applicationEventPublisher.publishEvent(new JoinChannelEvent(this, split[i]));
            }
    }

    @EventListener()
    public void execute(ProtectedMessageArrived parameter) {
        handleMessage(parameter.getMessage());
    }
}
