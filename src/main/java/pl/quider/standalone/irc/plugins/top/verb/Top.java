package pl.quider.standalone.irc.plugins.top.verb;

import lombok.extern.slf4j.Slf4j;
import pl.quider.standalone.irc.bot.model.Channel;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.verb.Verb;

@Slf4j
public class Top extends Verb {

    public void execute(Message parameter) throws Exception {
//        Configuration configuration = Configuration.getInstance();
//        if(!configuration.getValue(ConfigurationKeysConstants.TOP_ENABLED).equals("1")){
//            return;
//        }
//
//        ChannelService channelService = new ChannelService(bot.getSession());
//        List<Channel> topStats = channelService.getTopStats(msg.getChannel());
//        for (int i = 0; i <= 4; i++) {
//            Channel channel = topStats.get(i);
//            this.write(prepareLine(i,channel));
//        }
//        //TODO: where asking person is in table
    }

    /**
     *
     * @param i
     * @param channel
     * @return
     */
    protected String prepareLine(int i, Channel channel) {
        StringBuilder stringStat = new StringBuilder(Integer.toString(i + 1)).append(". ");
        stringStat.append(channel.getUser().getNickName());
        stringStat.append(" - ");
        stringStat.append(channel.getWordCount()).append(" słów.");
        stringStat.append(" (").append(channel.getUser().getLogin()).append("@").append(channel.getUser().getMask()).append(")");
        return  stringStat.toString();
    }

    /**
     *
     * @param stringStat
     */
    protected void write(String stringStat) {
//        bot.sendMessage(msg.getChannel(), stringStat);
    }
}
