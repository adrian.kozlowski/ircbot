package pl.quider.standalone.irc.plugins.op.verb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.quider.standalone.irc.bot.events.MessageArrived;
import pl.quider.standalone.irc.bot.events.MessageReadyToSend;
import pl.quider.standalone.irc.bot.events.OpUserEvent;
import pl.quider.standalone.irc.bot.exceptions.NoLoginException;
import pl.quider.standalone.irc.bot.services.UserService;
import pl.quider.standalone.irc.bot.verb.Verb;

@Slf4j
@Component
public class Op extends Verb {
    private boolean setOpBysomeOne = false;


    @Value("bot.op_enabled")
    private String opEnabled;

    @Autowired
    private UserService userService;

    @EventListener
    public void execute(MessageArrived msg) {
        if (!opEnabled.equals("1")) {
            return;
        }
        try {
            if (!this.setOpBysomeOne) {
                if (msg.getUser().isOp()) {
                    applicationEventPublisher.publishEvent(new OpUserEvent(this, msg.getMessage().getChannel(), msg.getUser().getNickName()));
                }
            } else {
                String[] split = msg.getMessage().getMessage().split(" ");
                if (split.length == 2 && split[0].equals("!bot") && split[1].equals("op"))
                    userService.opUser(split[2], split[1], split[3]);
                else
                    throw new NoLoginException();
            }
        } catch (NoLoginException e) {
            applicationEventPublisher.publishEvent(new MessageReadyToSend(this, msg.getMessage().getChannel(), "Powiedzmy, że wiem o co chodzi... ale mój pstryczek do opania się zepsuł... ;)"));
            e.printStackTrace();
        }
    }
}
