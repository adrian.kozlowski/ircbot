package pl.quider.standalone.irc.plugins.stats.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.quider.standalone.irc.bot.events.MessageArrived;
import pl.quider.standalone.irc.bot.model.Channel;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.repositories.ChannelRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StatService {

    @Autowired
    private ChannelRepository channelRepository;

    /**
     * Updates statistics of user in channel. Transaction included.
     *
     * @param event message which has been sent.
     */
    @EventListener
    public void updateStats(MessageArrived event) {
        String channelName = event.getMessage().getChannel();
        Optional<Channel> resultList = channelRepository.findByUserAndChannelName(event.getUser(), channelName);
        int countNewWords = getCountWordsFromMessage(event.getMessage());
        Channel channel = resultList.orElseGet(() ->
        {
            Channel chanInfo = new Channel();
            chanInfo.setWordCount(0);
            chanInfo.setChannelName(channelName);
            chanInfo.setUser(event.getUser());
            return chanInfo;
        });
        channel.addWordsCount(countNewWords);
        channelRepository.save(channel);
    }

    /**
     * Counts words which are in message sent by user.
     *
     * @param msg message object.
     * @return count of words in message.
     */
    protected int getCountWordsFromMessage(Message msg) {
        String[] split = msg.getMessage().split(" ");
        return split.length;
    }

    /**
     * @param msg message object
     * @return Message to channel
     */
    public int getStats(Message msg) {
        Optional<Channel> singleResult = channelRepository.findByUserAndChannelName(msg.getUser(), msg.getChannel());
        if (singleResult.isPresent()) {
            return singleResult.get().getWordCount();
        } else {
            return 0;
        }
    }

    /**
     * Gets stats from channel
     *
     * @param channel channel name
     * @return List of channels stats
     * @throws Exception in case of failure - exception
     */
    public List<Channel> getTopStats(String channel) throws Exception {
//        Query<Channel> query = session.createQuery("from Channel as c where channelName = :channel order by wordCount desc", Channel.class).setMaxResults(5);
//        query.setParameter("channel", channel);
//        return query.getResultList();
        return new ArrayList<>();
    }

}
