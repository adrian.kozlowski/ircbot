package pl.quider.standalone.irc.plugins.kiedy.verb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.quider.standalone.irc.bot.events.MessageArrived;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.util.Date;

/**
 * Class Kiedy is verb which shows users when bot has seen another nick last time
 */
@Component
@Slf4j
public class Kiedy extends Verb {


    public void execute(MessageArrived parameter) throws Exception {
//        log.info("Kiedy verb fired");
//        Configuration configuration = Configuration.getInstance();
//        if(!configuration.getValue(ConfigurationKeysConstants.KIEDY_ENABLED).equals("1")){
//            log.info("Kiedy disabled by configuration");
//            return;
//        }
//
//        final String[] split = parameter.split(MyBot.VERB_PARAM_DELIMITER);
//        if(split.length >1 || split.length == 0){
//            throw new ParameterMisuseException("Tylko jeden nick na raz");
//        }
//        log.debug("parameter: " + split[0].toString());
//        final UserService userService = new UserService(split[0], null, null, bot.getSession());
//        Date seen = null;
//        seen = userService.seen(split[0]);
//        this.sendMessage(seen, split[0]);
    }

    private void sendMessage(Date seen, String nickname) {
//        if(seen != null) {
//            StringBuilder sb = new StringBuilder();
//            sb.append("Ostatni raz ").append(nickname).append(" pisał(a) ").append(seen);
//            bot.sendMessage(msg.getChannel(), sb.toString());
//        } else {
//            bot.sendMessage(msg.getChannel(), "Nic nie mówi mi nick "+nickname);
//        }
    }
}
