package pl.quider.standalone.irc.plugins.lotto.verb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by Michal on 26.10.2016.
 */
@Component
@Slf4j
public class Lotto extends Verb {

    private final Map<Date, ArrayList<Integer>> allDrawings = new HashMap<>();
    private final DateFormat incomingDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
    private final static String resultsListSource = "http://www.mbnet.com.pl/dl.txt";


//    public Lotto(MyBot mybot, Message msg) {
//        super(mybot, msg);
//        this.loadAllDrawings();
//    }

    public void execute(Message params) throws Exception {
        if (this.allDrawings.size() > 0) {
            this.runLottery();
        } else {
//            bot.sendMessage(msg.getChannel(), "Lotto nie działa :(");
        }
    }

    /**
     * TODO: javadoc
     */
    private void loadAllDrawings() {
        URL url;
        Scanner s = null;
        try {
            url = new URL(this.resultsListSource);
            s = new Scanner(url.openStream());
            s.useDelimiter("\r\n");
        } catch (MalformedURLException e) {
            log.error("MalformedURLException " + e.fillInStackTrace());
        } catch (IOException e) {
            log.error("IOException " + e.fillInStackTrace());
        }

        while (s.hasNext()) {
            String singleLine = s.next();
            String[] drawingDetails = singleLine.split(" ");
            Date drawingDate = this.getDate(drawingDetails[1]);
            ArrayList<Integer> drawingNumbers = this.getDrawingNumbers(drawingDetails[2]);

            this.allDrawings.put(drawingDate, drawingNumbers);
        }
        log.info("Lotto loaded [" + this.allDrawings.size() + "] lottery drawings");
    }

    /**
     * Runs the lottery
     *
     * @return String response ready to be send
     */
    private final void runLottery() {
        ArrayList<Integer> randomNumbers = this.getRandomNumbers();

        String luckyNumbers = "";
        for (Integer i : randomNumbers) {
            luckyNumbers += " " + i;
        }

//        this.sendMessage(msg.getChannel(),"W kasecie maszyny losującej, znajduje się 49 kolejno ułożonych kul, bęben maszyny losującej jest pusty, zwolnienie blokady i rozpoczynamy losowanie 6 liczb...");
//        this.sendMessage(msg.getChannel(),"Twoje szczęśliwe liczby to: " + luckyNumbers);
//        this.sendMessage(msg.getChannel(),this.findMyLuckyDay(randomNumbers));
    }

    /**
     * Methods finds when "6" has beed hit
     *
     * @param luckyNumbers array of int numbers
     * @return String date
     */
    private final String findMyLuckyDay(ArrayList<Integer> luckyNumbers) {
        Date maximumScoreDate = new Date();
        ArrayList<Integer> maximumScoreNumbers = new ArrayList<>();
        int maximumMatch = 0;

        for (Map.Entry<Date, ArrayList<Integer>> entry : this.allDrawings.entrySet()) {
            int localMax = 0;
            for (int i = 0; i < luckyNumbers.size(); i++) {
                int checkingNum = luckyNumbers.get(i);
                if (entry.getValue().contains(checkingNum)) {
                    localMax++;
                    if (localMax > maximumMatch) {
                        maximumMatch = localMax;
                        maximumScoreDate = entry.getKey();
                        maximumScoreNumbers = entry.getValue();
                    }
                }
            }
        }

        String nmbs = maximumScoreNumbers.toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy - E");
        String date = sdf.format(maximumScoreDate);

        String res = "Trafiłeś *" + maximumMatch + "* " + nmbs + " @ " + date;
        return res;
    }

    /**
     * Gets numbers as parameter, split them and adds to collection of intigers
     *
     * @param inNumbers numbers comma seperated
     * @return array list of ints passed as parameter. Empty collection in case of failure.
     */
    private final ArrayList<Integer> getDrawingNumbers(final String inNumbers) {
        String[] drawingNumbersString = inNumbers.split(",");
        ArrayList<Integer> drawingNumbers = new ArrayList<>();

        for (String s : drawingNumbersString) {
            try {
                Integer singleNumber = Integer.parseInt(s);
                drawingNumbers.add(singleNumber);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        return drawingNumbers;
    }

    /**
     * Parses date which is passed as parameter
     *
     * @param inDate string date
     * @return date object or null in case of error.
     */
    private final Date getDate(final String inDate) {
        Date parsedDate = null;
        try {
            parsedDate = this.incomingDateFormat.parse(inDate);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return parsedDate;
    }

    /**
     * Gets random numbers from 1 to 49
     *
     * @return array list of 6 random number, sorted by value
     */
    private final ArrayList<Integer> getRandomNumbers() {
        ArrayList<Integer> randomNumbers = new ArrayList<>();
        int min = 1;
        int max = 49;
        int numbersToGenerate = 6;
        while (randomNumbers.size() != numbersToGenerate) {
            int num = ThreadLocalRandom.current().nextInt(min, max + 1);
            if (!randomNumbers.contains(num)) {
                randomNumbers.add(num);
            }
        }
        Collections.sort(randomNumbers);

        return randomNumbers;
    }
}
