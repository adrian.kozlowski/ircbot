package pl.quider.standalone.irc.plugins.rss.verb;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.services.EnshortLinkService;
import pl.quider.standalone.irc.bot.services.RssService;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.io.IOException;

@Component
@Slf4j
public class Rss extends Verb {

    public void execute(Message parameter) throws Exception {
//        log.info("RSS execute function");
//        Configuration configuration = Configuration.getInstance();
//        if(!configuration.getValue(ConfigurationKeysConstants.RSS_ENABLED).equals("1")){
//            log.info("Rss disabled by configuration");
//            return;
//        }
//        if (msg == null && parameter == null) {
//            try {
//                SyndFeedInput input = new SyndFeedInput();
//                RssService rssResvice = createServiceObject();
//                List<pl.quider.standalone.irc.bot.model.Rss> feeds = rssResvice.getListOfFeeds();
//                for (pl.quider.standalone.irc.bot.model.Rss rss : feeds) {
//                    HttpURLConnection httpcon = (HttpURLConnection) new URL(rss.getUri()).openConnection();
//                    SyndFeed feed = null;
//                    feed = input.build(new XmlReader(httpcon));
//                    for (Object syndEntry : feed.getEntries()) {
//                        SyndEntryImpl entry = (SyndEntryImpl) syndEntry;
//                        if (rssResvice.saveEntry(entry.getTitle(), entry.getUri(), rss)) {
//                            sendToChannels(entry, rss);
//                        }
//                    }
//                }
//            } catch (FeedException | IOException e) {
//                log.error(e.getMessage(), e);
//            }
//        }
    }

    /**
     * sends to all channels
     * @param entry
     * @param feed
     */
    private void sendToChannels(SyndEntryImpl entry, pl.quider.standalone.irc.bot.model.Rss feed) {
//        for (String channel : bot.getChannels()) {
//            StringBuilder sb = new StringBuilder();
//            sb.append("[").append(feed.getName()).append("] ");
//            sb.append( entry.getTitle()).append(" ; ");
//            sb.append("URL: ").append(Colors.BOLD).append(this.enshortlink(entry.getUri()));
//            this.sendMessage(channel, Colors.CYAN + sb.toString());
//        }
    }

    /**
     * Enshort link using Enshort service. In case of ecxeption returs input.
     * @param uri url to enshort
     * @return short url or input in case of error.
     */
    private String enshortlink(String uri) {
        EnshortLinkService enshortLinkService = new EnshortLinkService();
        try {
            return enshortLinkService.generateShortUrl(uri);
        } catch (IOException | DocumentException | SAXException e) {
            return  uri;
        }
    }

    /**
     * Create testable service object
     * @return RssService
     */
    protected RssService createServiceObject() {
//        return new RssService(bot.getSession());
        return null;
    }

    /**
     * Sends rss to all channels
     * @param channel channel where message should be send
     * @param text text to send
     */
    protected void sendMessage(String channel, String text) {
//        bot.sendMessage(channel, text);
    }
}
