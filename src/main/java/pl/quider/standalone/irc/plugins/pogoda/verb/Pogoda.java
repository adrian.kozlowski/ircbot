package pl.quider.standalone.irc.plugins.pogoda.verb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.verb.Verb;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Adrian on 12.10.2016.
 */
@Component
@Slf4j
public class Pogoda extends Verb {
   private static final String TEMPERATURE = "temperature";
   private static final String VALUE = "value";
   private static final String PRESSURE = "pressure";
   private static final String HUMIDITY = "humidity";
   private static final String POST = "POST";
   private String API_CODE = "&APPID=%api_code%&mode=xml";
   private static String URL = "http://api.openweathermap.org/data/2.5/forecast/city?q=";

//    public Pogoda(MyBot mybot, Message msg) throws MalformedURLException {
//        super(mybot, msg);
//
//        //dirty workaround...
//        Configuration configuration = Configuration.getInstance();
//        String apiKey = configuration.getValue(ConfigurationKeysConstants.API_WEATHER);
//        API_CODE = API_CODE.replace("%api_code%",apiKey);
//    }

    public void execute(Message parameter) throws Exception {
//        Configuration configuration = Configuration.getInstance();
//        if(!configuration.getValue(ConfigurationKeysConstants.WEATHER_ENABLED).equals("1")){
//            return;
//        }
//        String[] split = parameter.split(MyBot.VERB_PARAM_DELIMITER);
//        if (split.length == 1) {
//            try {
//                InputStream is = getInputStream(split);
//
//                NodeList childNodes = getNodeList(is);
//                StringBuilder sbMessage = getStringBuilder(childNodes);
//
//                String s = sbMessage.toString();
//                this.sendMessage(s);
//            } catch (IOException e) {
//                sendMessage("Em... Coś nie halo...");
//                e.printStackTrace();
//            } catch (ParserConfigurationException e) {
//                sendMessage("Konfiguracja nie bangla");
//                e.printStackTrace();
//            } catch (SAXException e) {
//                sendMessage("Xml z webserwisu jest do bani");
//                e.printStackTrace();
//            } catch (DOMException e) {
//                sendMessage("Rozjechało się parsowanie");
//                e.printStackTrace();
//            }
//        }
    }

    /**
     * Fetches input stream from request
     * @param split array of connection string parts
     * @return input stream
     * @throws IOException in case of failure
     */
    protected InputStream getInputStream(String[] split) throws IOException {
        java.net.URL url = new URL(URL + split[0] + API_CODE);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod(POST);
        return request.getInputStream();
    }

    /**
     *
     * @param childNodes nodes
     * @return Ready to send message with weather forecast.
     */
    protected StringBuilder getStringBuilder(NodeList childNodes) {
        StringBuilder sbMessage= new StringBuilder();

        for (int i = 0; i <= childNodes.getLength() - 1; i++) {
            Node item = childNodes.item(i);
            if (item.getNodeName().equals(TEMPERATURE)) {
                Node value = item.getAttributes().getNamedItem(VALUE);
                sbMessage.append("Temp: ").append(value.getNodeValue()).append("*C, ");
            } else if (item.getNodeName().equals(PRESSURE)) {
                Node value = item.getAttributes().getNamedItem(VALUE);
                sbMessage.append("Ciśnienie: ").append(value.getNodeValue()).append("hPa, ");
            } else if (item.getNodeName().equals(HUMIDITY)) {
                Node value = item.getAttributes().getNamedItem(VALUE);
                sbMessage.append("Wilgotność: ").append(value.getNodeValue()).append("%.");
            }
        }
        return sbMessage;
    }

    /**
     *
     * @param is input stream (can be fetched from other function)
     * @return NodeList
     * @throws ParserConfigurationException in case of failure of parsing
     * @throws SAXException xml exception
     * @throws IOException file problems
     */
    protected NodeList getNodeList(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(is);
        Element root = document.getDocumentElement();
        NodeList forecast = root.getElementsByTagName("forecast");
        Node firstChild = forecast.item(0).getFirstChild();
        return firstChild.getChildNodes();
    }

    /**
     * Sends message.
     * @param message message which will be sent
     */
    protected void sendMessage(String message) {
//        bot.sendMessage(msg.getChannel(), message);
    }


}
