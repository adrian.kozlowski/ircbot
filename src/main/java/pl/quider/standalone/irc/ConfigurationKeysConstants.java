package pl.quider.standalone.irc;

public interface ConfigurationKeysConstants {
    String NICK = "nick";
    String SERVER1 = "server1";
    String ALT_NICK = "alt_nick";
    String LOGIN = "login";
    String JOIN_CHANNEL = "join_channel";
    String RSS_ENABLED = "rss_enabled";
    String OWNER = "owner";
    String SHORTLINK_ENABLED = "short_link_enabled";
    String TOP_ENABLED = "top_enabled";
    String STATS_ENABLED = "stats_enabled";
    String OP_ENABLED = "op_enabled";
    String JOIN_ENABLED = "join_enabled";
    String KIEDY_ENABLED = "kiedy_enabled";
    String WEATHER_ENABLED = "pogoda_enabled";
    String API_WEATHER = "api_pogoda";
    String APPLICATION_DB_TYPE = "application.db.type";
}
