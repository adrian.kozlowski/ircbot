package pl.quider.standalone.irc;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.adriankozlowski.irc.IrcException;
import pl.adriankozlowski.irc.NickAlreadyInUseException;
import pl.quider.standalone.irc.bot.MyBot;

import java.io.IOException;

@ComponentScan({"pl.quider.standalone.irc", "pl.quider.standalone.irc.bot"})
@Configuration
@Slf4j
@PropertySource("application.properties")
@EnableJpaRepositories
public class Main {

    private static ApplicationContext ctx;

    /**
     * before we run database connection and other parts
     * first read parameters:
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            ctx = new AnnotationConfigApplicationContext(Main.class);
        } catch (Exception e) {
            log.error("EXITING!");
            log.error(e.getMessage(), e);
            System.exit(10);
        }
    }

    private static void help() {
        System.out.println("To chose logback.xml location run application with -Dlog4j.configurationFile=path/to/logback.xml parameter");
    }

    @Bean
    public MyBot myBot(@Value("${irc.nick}")
                               String nick,
                       @Value("${irc.servers}")
                               String servers,
                       @Value("${irc.join.channel}")
                               String channel,
                       @Value("${irc.alt.nick}")
                               String altNick) {
        MyBot bot = new MyBot();
        boolean isAltNickUsed = false;
        log.info("Create bot instance");
        while (!bot.isConnected()) {
            bot.setVerbose(true);
            bot.changeNick(nick);
            log.info("nick={}", bot.getNick());
            try {
                log.info("Only one server is supported for now.");
                log.info("server={}", servers);
                bot.connect(servers);
                log.info("channel={}", channel);
                bot.joinChannel(channel);
            } catch (NullPointerException e) {
                log.error(e.getMessage());
                log.info("Braking connection loop. Application is being closed.");
                System.exit(1);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                log.info("Braking connection loop. Application is being closed.");
                System.exit(1);
            } catch (NickAlreadyInUseException e) {
                if (isAltNickUsed) {
                    bot.changeNick(altNick);
                    isAltNickUsed = true;
                } else {
                    altNick = altNick.concat("_");
                    bot.changeNick(altNick);
                }
                log.error("bot not initialized but change name is about to perform");
                log.error(e.getMessage(), e);
            } catch (IrcException e) {
                log.error(e.getMessage(), e);
                log.info("Braking connection loop. Application is being closed.");
                System.exit(1);
            }
        }
        return bot;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
