package pl.quider.standalone.irc.bot.services;

import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

/**
 * Created by Adrian on 06.11.2016.
 */
public class EnshortLinkService {

    private static String URL = "http://xurl.pl/api/v1/56a1d83fb78777992fd0eca25ff85d29/shorturl/create/URL/";


    public String generateShortUrl(String stringUrl) throws IOException, DocumentException, SAXException {
        URL theUrl = setUrl(stringUrl);
        InputStream is = getInputStream(theUrl);
        String url = getElement(is);
        return url;
    }

    /**
     *
     * @param is Input Stream
     * @return String of response
     * @throws SAXException Xml error
     * @throws IOException file exception
     */
    protected String getElement(InputStream is) throws SAXException, IOException, DocumentException {
        SAXReader reader = new SAXReader();
        org.dom4j.Document document = reader.read(is);
        Node node = document.selectSingleNode("//message/response/data/short_url");

        return node.getText();
    }

    /**
     * Gets input stream of URL
     * @param theUrl URL to get stream of
     * @return input stream
     * @throws IOException in case of failure
     */
    protected InputStream getInputStream(URL theUrl) throws IOException, DocumentException {
        HttpURLConnection urlConnection = (HttpURLConnection) theUrl.openConnection();
        urlConnection.setRequestMethod("GET");
        return urlConnection.getInputStream();
    }

    /**
     * Using base 64 encodes long URL and passes to request URL
     * @return request URL
     * @throws MalformedURLException in case of failure
     */
    public URL setUrl(String urlString) throws MalformedURLException {
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(urlString.getBytes());
        String s = new String(encode);
        return new URL(URL + s + ".xml");
    }
}
