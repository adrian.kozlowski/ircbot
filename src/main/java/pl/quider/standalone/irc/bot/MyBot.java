package pl.quider.standalone.irc.bot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.adriankozlowski.irc.PircBot;
import pl.adriankozlowski.irc.ReplyConstants;
import pl.adriankozlowski.irc.User;
import pl.quider.standalone.irc.bot.events.JoinChannelEvent;
import pl.quider.standalone.irc.bot.events.MessageArrived;
import pl.quider.standalone.irc.bot.events.OpUserEvent;
import pl.quider.standalone.irc.bot.events.ProtectedMessageArrived;
import pl.quider.standalone.irc.bot.exceptions.NoLoginException;
import pl.quider.standalone.irc.bot.model.Channel;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.repositories.UserRepository;
import pl.quider.standalone.irc.bot.services.ChannelService;
import pl.quider.standalone.irc.bot.services.MessageService;
import pl.quider.standalone.irc.bot.services.RabbitService;
import pl.quider.standalone.irc.bot.services.UserService;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.util.List;

@Slf4j
@Component
public class MyBot extends PircBot implements ApplicationContextAware, ApplicationEventPublisherAware {

    /**
     * 170.178.184.37
     * <p>
     * delimiter used mostly in parameter in verb
     */
    public static final String VERB_PARAM_DELIMITER = "&%%&";

    /**
     * Interval in seconds. Used in scheduled executor.
     */
    public static final int INT = 240;

    boolean isOp;
    private Verb verb;

    @Value("${irc.nick}")
    private String nick;
    @Value("${irc.servers}")
    private String servers;
    @Value("${irc.join.channel}")
    private String channel;
    @Value("${irc.alt.nick}")
    private String altNick;

    @Autowired
    private UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    private ChannelService channelService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private RabbitService rabbitService;

    private ApplicationContext applicationContext;
    private ApplicationEventPublisher eventPublisher;

    /**
     * @param autoNickChange Set to true if you want automatic nick changes
     */
    @Override
    public void setAutoNickChange(boolean autoNickChange) {
        super.setAutoNickChange(autoNickChange);
    }

    /**
     * @inheritDoc
     */
    @Override
    protected void onDisconnect() {
        log.info("Application got disconnected");
        try {
            log.info("Reconnecting to servers={}", servers);
            connect(servers);
            log.info("Creating ChannelService");

            List<Channel> list = channelService.joinChannels();
            //noinspection unchecked
            list.forEach(item -> {
                log.info("Try to join channel " + item);
                joinChannel(item.getChannelName());
            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * @param code     The three-digit numerical code for the response.
     * @param response The full response from the IRC server.
     * @inheritDoc
     */
    @Override
    protected void onServerResponse(int code, String response) {
        log.debug("server respond: code={}; response={} ", code, response);
        switch (code) {
            case ReplyConstants.RPL_WHOISUSER:
                if (verb != null) {
                    log.debug("verb:{}", verb.getClass().getName());
                    try {
                        //todo: figure it out
//                        verb.execute(response);
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    verb = null;
                }
                break;
        }
    }

    @Override
    protected void onProtectedMessage(String sender, String login, String hostname, String message) {
        log.info("[privmsg] <{}> :{}", sender, message);
        pl.quider.standalone.irc.bot.model.User user = userService.getUser(login, sender, hostname);
        Message msg = new Message(sender, sender, user, message);
        messageService.save(msg);
        eventPublisher.publishEvent(new ProtectedMessageArrived(this, msg));
    }

    /**
     * @param channel The name of the channel.
     * @param users   An array of User objects belonging to this channel.
     * @inheritDoc
     */
    @Override
    protected void onUserList(String channel, User[] users) {
        for (User usr : users) {
            log.debug("Checking user list: {}", usr.getNick());
            String nick = usr.getNick();
            if (usr.isOp()) {
                eventPublisher.publishEvent(new OpUserEvent(this, channel, usr.getNick()));
            }
        }
    }

    /**
     * @param channel  The channel to which the message was sent.
     * @param sender   The nick of the person who sent the message.
     * @param login    The login of the person who sent the message.
     * @param hostname The hostname of the person who sent the message.
     * @param message  The actual message sent to the channel.
     */
    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {
        try {
            pl.quider.standalone.irc.bot.model.User user = userService.getUser(login, sender, hostname);

            Message msg = new Message(channel, sender, user, message);
            log.info(" [{}] <{}> :{}", msg.getChannel(), sender, msg.getMessage());
            messageService.save(msg);
            rabbitService.publishMessage(msg);
            eventPublisher.publishEvent(new MessageArrived(this, msg, user));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Creates user service object and marks users presence, ops user if
     * he/she has such right.
     *
     * @param channel  The channel which somebody joined.
     * @param sender   The nick of the user who joined the channel.
     * @param login    The login of the user who joined the channel.
     * @param hostname The hostname of the user who joined the channel.
     */
    @Override
    protected void onJoin(String channel, String sender, String login, String hostname) {
        try {
            log.info(" <{}> joined to {}", sender, channel);
            userService.joined(channel, sender, hostname);
            pl.quider.standalone.irc.bot.model.User user = userService.getUser(login, sender, hostname);
            if (user.isOp()) {
                log.info("{} is op by configuration", sender);
                op(channel, sender);
            }
        } catch (NoLoginException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Sends WHOIS and after response finds a user in
     * database and op it
     *
     * @param recipient nick which will be opped
     */
    @EventListener
    public void opNick(OpUserEvent recipient) {
        sendRawLine("WHOIS " + recipient);
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @EventListener
    public void joinChannelEvent(JoinChannelEvent event) {
        joinChannel(event.getChannel());
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
}
