package pl.quider.standalone.irc.bot.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.repositories.MessageRepository;
import pl.quider.standalone.irc.bot.verb.Verb;

import java.lang.reflect.InvocationTargetException;

@Service
@Slf4j
public class MessageService implements ApplicationContextAware {

    @Autowired
    private MessageRepository messageRepository;

    private ApplicationContext ctx;


    protected void enshortLinks(Message msg) {
//        String message = msg.getMessage();
//        if ((message.startsWith("http://") || message.startsWith("https://"))&&(message.length()>101)) {
//
//            EnshortLinkFiler enshortLinkFiler = new EnshortLinkFiler(msg, mybot);
//            enshortLinkFiler.execute();
//        }
    }

    /**
     * @param verbClassName
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    protected Verb factorVerb(final String verbClassName) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Verb bean = (Verb) ctx.getBean(verbClassName.toLowerCase());
        return bean;
    }

    /**
     * checks if user call bot
     *
     * @return true or false
     */
    public boolean isUserCallingMe(String message) {
        return message.startsWith("!bot ");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    /**
     * Saves message to database
     *
     * @param msg
     */
    public void save(Message msg) {
        messageRepository.save(msg);
    }
}
