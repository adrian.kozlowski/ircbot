package pl.quider.standalone.irc.bot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.quider.standalone.irc.bot.model.Channel;
import pl.quider.standalone.irc.bot.repositories.ChannelRepository;

import java.util.List;

@Service
public class ChannelService {

    @Autowired
    private ChannelRepository channelRepository;

    /**
     * fetches to all channels which are mentioned in table channel
     *
     * @return list of channel names
     */
    public List<Channel> joinChannels() throws Exception {
        return channelRepository.findAll();
    }

}
