package pl.quider.standalone.irc.bot.events;

import org.springframework.context.ApplicationEvent;

public class JoinChannelEvent extends ApplicationEvent {
    private String channel;

    public JoinChannelEvent(final Object source, final String channel) {
        super(source);
        this.channel = channel;
    }

    /**
     * returns channel name with # at beggining
     *
     * @return #channelName
     */
    public String getChannel() {
        return channel.startsWith("#") ? channel : "#" + channel;
    }
}
