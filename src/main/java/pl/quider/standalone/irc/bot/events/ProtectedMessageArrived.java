package pl.quider.standalone.irc.bot.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import pl.quider.standalone.irc.bot.model.Message;

@Getter
public class ProtectedMessageArrived extends ApplicationEvent {
    private final Message message;

    public ProtectedMessageArrived(Object source, Message msg) {
        super(source);
        this.message = msg;
    }
}
