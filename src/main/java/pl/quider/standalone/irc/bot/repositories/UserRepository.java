package pl.quider.standalone.irc.bot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.quider.standalone.irc.bot.model.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    //    @Query("from pl.quider.standalone.irc.bot.model.User as u where u.mask = :host and u.login = :login")
    Optional<User> findUserByMaskAndLogin(String mask, String login);

    Optional<User> findUserByNickName(String nickName);
}
