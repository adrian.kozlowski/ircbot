package pl.quider.standalone.irc.bot.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import pl.quider.standalone.irc.bot.MyBot;

@Getter
public class BotReadyToConnect extends ApplicationEvent {
    private final MyBot myBot;

    public BotReadyToConnect(MyBot myBot) {
        super(myBot);
        this.myBot = myBot;
    }
}
