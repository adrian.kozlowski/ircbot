package pl.quider.standalone.irc.bot.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.quider.standalone.irc.bot.model.Message;

@Service
public class RabbitService {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private TopicExchange topic;

    @Autowired
    private ObjectMapper objectMapper;

    public void publishMessage(Message messageToPublish) throws JsonProcessingException {
        byte[] bytes = objectMapper.writeValueAsBytes(messageToPublish);
        template.send(topic.getName(), "messages.all", new org.springframework.amqp.core.Message(bytes,
                new MessageProperties()));
    }
}
