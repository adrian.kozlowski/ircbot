package pl.quider.standalone.irc.bot.events;

import org.springframework.context.ApplicationEvent;

public class OpUserEvent extends ApplicationEvent {
    public OpUserEvent(Object verb, String channel, String nickName) {
        super(verb);
    }
}
