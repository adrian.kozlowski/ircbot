package pl.quider.standalone.irc.bot.events;

import org.springframework.context.ApplicationEvent;
import pl.quider.standalone.irc.bot.verb.Verb;

public class MessageReadyToSend extends ApplicationEvent {
    public MessageReadyToSend(Verb verb, String channel, String s) {
        super(verb);
    }
}
