package pl.quider.standalone.irc.bot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.quider.standalone.irc.bot.model.Channel;
import pl.quider.standalone.irc.bot.model.User;

import java.util.Optional;

public interface ChannelRepository extends JpaRepository<Channel, Integer> {
    Optional<Channel> findByUserAndChannelName(User user, String channel);
}
