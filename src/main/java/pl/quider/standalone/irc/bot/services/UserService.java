package pl.quider.standalone.irc.bot.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pl.quider.standalone.irc.bot.exceptions.NoLoginException;
import pl.quider.standalone.irc.bot.model.User;
import pl.quider.standalone.irc.bot.repositories.UserRepository;

import java.util.Date;
import java.util.Optional;

@Slf4j
@Service
@Scope("prototype")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Gets user from database or creates new.
     *
     * @return
     */
    public User getUser(String login, String nick, String host) {
        if (login == null || login.isEmpty()) {
            throw new NoLoginException();
        }
        Optional<User> hipoteticUser = userExists(login, host);
        if (!hipoteticUser.isPresent()) {
            return createNewUser(nick, login, host);
        }
        return hipoteticUser.orElseThrow();
    }

    /**
     * Creates new user in database and sets it to propery.
     */
    public User createNewUser(String nick, String login, String host) {
        User user = new User();
        user.setNickName(nick);
        user.setLogin(login);
        user.setMask(host);
        userRepository.save(user);
        return user;
    }

    /**
     * Checks if user exists in database.
     *
     * @return user object if exists. Otherwise null.
     */
    public Optional<User> userExists(String login, String host) {
        log.info("Checking if user={} exists on host={}", login, host);
        Optional<User> user = userRepository.findUserByMaskAndLogin(host, login);
        //TODO: check by nick instead of host
        return user;
    }


    /**
     * Actions which supposed to be performed when someone joins to channel
     * like set presence
     */
    public void joined(String login, String nick, String host) throws NoLoginException {
        User user = getUser(login, nick, host);
        setUserPresence(user);
    }

    /**
     * Sets latest presence of user
     *
     * @param user
     */
    public void setUserPresence(User user) {
        user.setLastSeen(new Date());
        userRepository.save(user);
    }

    public void opUser(String login, String nick, String host) throws NoLoginException {
        User user = getUser(login, nick, host);
        user.setOp(true);
        userRepository.save(user);
    }

    /**
     * Checks in database when the passed nick was seen last time on channel
     * and returns the date of last seen.
     *
     * @param nickName nick to check
     * @return date when nick was seen whenever or null when it wasn't
     */
    public Date seen(String nickName) {
        Optional<User> singleResult = userRepository.findUserByNickName(nickName);
        return singleResult.orElseThrow().getLastSeen();
    }


    public void updateStats(String message) throws NoSuchMethodException {
        throw new NoSuchMethodException("not implemented yet");
    }
}
