package pl.quider.standalone.irc.bot.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import pl.quider.standalone.irc.bot.model.Message;
import pl.quider.standalone.irc.bot.model.User;

@Getter
public class MessageArrived extends ApplicationEvent {

    private final Message message;
    private final User user;

    public MessageArrived(Object source, Message message, User user) {
        super(source);
        this.user = user;
        this.message = message;
    }
}
