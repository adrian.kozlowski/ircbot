package pl.quider.standalone.irc.bot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.quider.standalone.irc.bot.model.Message;

public interface MessageRepository extends JpaRepository<Message, Integer> {
}
